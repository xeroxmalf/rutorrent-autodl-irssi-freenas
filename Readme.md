# xeroxmalf/rutorrent-autodl-irssi

Popular rtorrent client with a webui for ease of use [Rutorrent](https://github.com/Novik/ruTorrent), integrated with [autodl-irssi](https://github.com/autodl-community/autodl-irssi)

This container is based on the rutorrent docker container from the fantastic folks over at [LinuxServer.io](https://linuxserver.io)

## Usage

```
docker create --name=rtorrent \
--network=host \
--restart="always" \
-v /some/config/dir:/config \
-v /some/downloads/dir:/downloads \
-v /some/postprocess/dir:/postprocess \
-e PGID=1000 -e PUID=1000 -e TZ=America/Thunder_Bay \
xeroxmalf/rutorrent-autodl-irssi
```
## Parameters

* `-v /config` - where rutorrent should store it's config files
* `-v /downloads` - path to your downloads folder
* `-v /postprocess` - path to where nzbtomedia will postprocess / copy / extract to
* `-e PGID` for GroupID - see below for explanation
* `-e PUID` for UserID - see below for explanation
* `-e TZ` for timezone information, eg Europe/London

### User / Group Identifiers

Sometimes when using data volumes (`-v` flags) permissions issues can arise between the host OS and the container. We avoid this issue by allowing you to specify the user `PUID` and group `PGID`. Ensure the data volume directory on the host is owned by the same user you specify and it will "just work" ™.

In this instance `PUID=1001` and `PGID=1001`. To find yours use `id user` as below:

```
  $ id <dockeruser>
    uid=1001(dockeruser) gid=1001(dockergroup) groups=1001(dockergroup)
```

## Setting up the application

Webui can be found at `https://<your-ip>` , configuration files are in /config/rtorrent.

`** Important note for unraid users or those running a webserver on port 80, change port 80 assignment
**`


## Updates

* Shell access whilst the container is running: `docker exec -it rtorrent /bin/bash`
* Upgrade to the latest version: `docker restart rtorrent`
* To monitor the logs of the container in realtime: `docker logs -f rtorrent`

## Versions

+ **09.10.2018:** Rebased to Alpine 3.8, added nzbtomedia, upgraded libtorrent/rtorrent to 0.13.7/0.9.7, upgraded mediainfo to 18.08.1, small bugfix to handle ungraceful shutdowns
+ **19.02.2017:** Downgraded curl to 7.50.0 and built it from source to get it to play nicely with SSL trackers using TLS v1.2
+ **04.02.2017:** Changed rutorrent to pull from git in the initial image instead of 3.7 zip.
+ **02.02.2017:** Added SSL and enabled by default on port 443.
+ **30.01.2017:** Initial Release.